<?php
//ini_set('display_errors', 1);
error_reporting(0);
session_start();
if(!$_SESSION['userbackend'] and !$_COOKIE['userbackend']){ header("location: index.php?error=3333"); exit(); } 
include("libreria/iniciar.php"); include('libreria/admin.php'); $ad = new admin();
$idadmin = $_SESSION['userbackend'] ? $_SESSION['userbackend'] : $_COOKIE['userbackend'];
$sql = $ini->consulta("select * from user_rols where rol_id = '1' and user_id = '".$idadmin."'"); if($ini->num_rows($sql) > 0){ $lvl = 1; }
$sql = $ini->consulta("select * from user_rols where rol_id = '2' and user_id = '".$idadmin."'"); if($ini->num_rows($sql) > 0){ $lvl = 2; }
$sql = $ini->consulta("select * from user_rols where rol_id = '3' and user_id = '".$idadmin."'"); if($ini->num_rows($sql) > 0){ $lvl = 3; }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es" xml:lang="es" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Panel administrador : GreenUnited</title>
<meta name="robots" content="noindex, nofollow"> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="libreria/style.css" type="text/css" rel="stylesheet">
<?php if(file_exists("cabecera/".$ad->htmlop($_GET['op']).".php")){ include("cabecera/".$ad->htmlop($_GET['op']).".php"); } ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="libreria/bootstrap.min.css" rel="stylesheet">
<link href="libreria/metisMenu.min.css" rel="stylesheet">
<link href="libreria/timeline.css" rel="stylesheet">
<link href="libreria/startmin.css" rel="stylesheet">
<link href="libreria/morris.css" rel="stylesheet">
<link href="libreria/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>
<script src="libreria/bootstrap.min.js"></script>
<script src="libreria/metisMenu.min.js"></script>
<script src="libreria/startmin.js"></script>
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">       
        <div class="navbar-header dasboar">
           <i class="fa fa-map-signs fa-fw"></i> <font class="fontb">GreenUnited</font>
        </div>        
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Menu movil</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <ul class="nav navbar-right navbar-top-links">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>  <b class="caret"></b>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="admin.php?op=1&se=4&id=<?php echo $idadmin; ?>"><i class="fa fa-gear fa-fw"></i> Configurar</a></li><li class="divider"></li>
                    <li><a href="libreria/unloggin.php?user=adm"><i class="fa fa-sign-out fa-fw"></i> Desconectar</a></li>
                </ul>
            </li>
        </ul>
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="user-profile">
                        <div class="dropdown user-pro-body">
                        	<img src="<?php $imgurl = $ini->selectcampo($idadmin,'users','url'); if($imgurl == ""){ echo 'http://localhost:8888/greenend/images/users/icon-user-default.png'; }else{ echo $imgurl; } ?>" class="img-circle">
                        	<span><font><b>Bienvenido</b> <?php echo $ini->selectcampo($idadmin,'users','name'); ?></font></span>                        	
                        </div>
                    </li>
                    
                    <?php if($lvl > 2){ ?>
                    <li>
                        <a href="admin.php" class="active"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <?php } ?>
                    
                    <?php if($lvl > 2){ ?>
                    <li>
                        <a href="admin.php?op=1&se=1"><i class="fa fa-user fa-fw"></i> Usuarios<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="admin.php?op=1&se=1"><i class="fa fa-eye fa-fw"></i> Ver Usuarios</a></li>
                            <li><a href="admin.php?op=1&se=2"><i class="fa fa-terminal fa-fw"></i> Crear Usuarios </a></li>
                            <li style="display:none;"><a href="admin.php?op=1&se=3">no</a></li>
                            <li style="display:none;"><a href="admin.php?op=1&se=4">no</a></li>                          
                        </ul>
                    </li>
                    <?php } ?>
                    
                    <?php if($lvl > 2){ ?>
                    <li>
                        <a href="admin.php?op=2&se=1"><i class="fa fa-shopping-cart fa-fw"></i> Pedidos<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="admin.php?op=2&se=1"><i class="fa fa-eye fa-fw"></i> Ver pedidos</a></li>                  
                        </ul>
                    </li>
                    <?php } ?>
                    
                    <?php if($lvl > 2){ ?>
					<!--<li>
						<a href="#"><i class="fa fa-exchange fa-fw"></i> Transferencias<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="admin.php?op=3&se=1"><i class="fa fa-eye fa-fw"></i> Ver transferencias</a></li>
						</ul>
					</li>-->
					<?php } ?>
					
					<?php if($lvl > 2){ ?>
                    <li>
                        <a href="#"><i class="fa fa-ticket fa-fw"></i> Productos en liquidación<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="admin.php?op=4&se=1"><i class="fa fa-eye fa-fw"></i> Ver P. liquidación</a></li>
                            <li><a href="admin.php?op=4&se=2"><i class="fa fa-terminal fa-fw"></i> Crear P. liquidación </a></li> 
                            <li style="display:none;"><a href="admin.php?op=4&se=3">no</a></li>
                            <li style="display:none;"><a href="admin.php?op=4&se=4">no</a></li>
                            <li style="display:none;"><a href="admin.php?op=4&se=5">no</a></li>
                        </ul>
                    </li>
                    <?php } ?>
                    
                    <?php if($lvl > 2){ ?>
                    <li>
                        <a href="#"><i class="fa fa-superscript fa-fw"></i> Green stocks<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="admin.php?op=5&se=1"><i class="fa fa-eye fa-fw"></i> Ver G. stocks</a></li>
                            <li><a href="admin.php?op=5&se=2"><i class="fa fa-terminal fa-fw"></i> Crear G. stocks </a></li>
                            <li style="display:none;"><a href="admin.php?op=5&se=3">no</a></li>
                            <li style="display:none;"><a href="admin.php?op=5&se=4">no</a></li>
                            <li style="display:none;"><a href="admin.php?op=5&se=5">no</a></li>
                        </ul>
                    </li>
                    <?php } ?>
                    
                    <?php if($lvl > 2){ ?>
                    <li>
                        <a href="#"><i class="fa fa-list-alt fa-fw"></i> Listas de importación<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="admin.php?op=6&se=1"><i class="fa fa-eye fa-fw"></i> Ver L. importación</a></li>
                            <li><a href="admin.php?op=6&se=2"><i class="fa fa-terminal fa-fw"></i> Crear L. importación </a></li> 
                            <li style="display:none;"><a href="admin.php?op=6&se=3">no</a></li>
                            <li style="display:none;"><a href="admin.php?op=6&se=4">no</a></li>
                            <li style="display:none;"><a href="admin.php?op=6&se=5">no</a></li>
                        </ul>
                    </li>
                    <?php } ?>
                    
                    <?php if($lvl > 2){ ?>
                    <li>
                        <a href="#"><i class="fa fa-users fa-fw"></i> Grupos de compra<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="admin.php?op=7&se=1"><i class="fa fa-eye fa-fw"></i> Ver G. compra</a></li>
                            <li><a href="admin.php?op=7&se=2"><i class="fa fa-terminal fa-fw"></i> Crear G. compra </a></li>
                            <li style="display:none;"><a href="admin.php?op=7&se=3">no</a></li>
                            <li style="display:none;"><a href="admin.php?op=7&se=4">no</a></li>
                            <li style="display:none;"><a href="admin.php?op=7&se=5">no</a></li>
                        </ul>
                    </li>
                    <?php } ?>

                </ul>
            </div>
        </div>
    </nav>
    
    
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php if($_GET['op'] != ""){
                    	  echo $ad->htmlopb($_GET['op']);
                    	 }else{
                    	 	 echo $ad->htmlopb("ppal"); 
                    	 	 } ?></h1>
                </div>
            </div>
			<?php if($_GET['msj'] != ""){ ?>
			<div class="alert <?php echo $ad->classmsj($_GET['msj']); ?> alert-dismissable">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php  echo $ad->msjusu($_GET['msj']); ?>
			</div>
			<?php }else{ ?>
			<div style="height:100px;"></div>
			<?php } ?>
			<?php  if(file_exists("html/".$ad->htmlop($_GET['op']).".php")){
				   include("html/".$ad->htmlop($_GET['op']).".php");
			       }else{ ?>
		    <?php $sql = $ini->consulta("select * from users"); $num = $ini->num_rows($sql); ?>
			<div class="form-group row">
				<div class="col-sm-3">
					<div class="caja-inicio">
						<p><font color="#000">Usuarios</font></p>
						<p><font color="#005952" size="6"><?php echo $num; ?></font></p>
						<?php 
						$sql = $ini->consulta("select distinct countries.name, countries.id from users inner join cities on users.city_id = cities.id inner join countries on cities.country_id = countries.id"); 
						while($reg = $ini->fetch_object($sql)){
						
							$ssql = $ini->consulta("select countries.name, countries.id from users inner join cities on users.city_id = cities.id inner join countries on cities.country_id = countries.id where countries.id = '".$reg->id."'");
							$nnum = $ini->num_rows($ssql);
							?><p><font size="1"><?php echo $reg->name." ".$nnum; ?></font></p><?php
						}
						?>   
					</div>
				</div>
				<div class="col-sm-3">
					<div class="caja-inicio">
						<p><font color="#000">Stocks</font></p>
						<?php $sql = $ini->consulta("select * from products"); $num = $ini->num_rows($sql); ?>
						<p><font color="#005952" size="6"><?php echo $num; ?></font></p>
						<?php $sql = $ini->consulta("select * from products where product_type_id = '2'"); $num = $ini->num_rows($sql); ?>
						<p><font size="1">P. Liquidación <?php echo $num; ?></font></p>
						<?php $sql = $ini->consulta("select * from products where product_type_id = '3'"); $num = $ini->num_rows($sql); ?>
						<p><font size="1">Green stocks <?php echo $num; ?></font></p>
						<?php $sql = $ini->consulta("select * from products where product_type_id = '4'"); $num = $ini->num_rows($sql); ?>
						<p><font size="1">Listas importación <?php echo $num; ?></font></p>
						<?php $sql = $ini->consulta("select * from products where product_type_id = '1'"); $num = $ini->num_rows($sql); ?>
						<p><font size="1">Grupos de compra <?php echo $num; ?></font></p>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="caja-inicio">
						<p><font color="#000">Pedidos</font></p>
						<?php $sql = $ini->consulta("select * from pedidos"); $num = $ini->num_rows($sql); ?>
						<p><font color="#005952" size="6"><?php echo $num; ?></font></p>
						<?php $sql = $ini->consulta("select * from pedidos where estado = '1'"); $num = $ini->num_rows($sql); ?>
						<p><font size="1">Finalizado <?php echo $num; ?></font></p> 
						<?php $sql = $ini->consulta("select * from pedidos where estado = '2'"); $num = $ini->num_rows($sql); ?>
						<p><font size="1">En tramite <?php echo $num; ?></font></p>
						<?php $sql = $ini->consulta("select * from pedidos where estado = '3'"); $num = $ini->num_rows($sql); ?>
						<p><font size="1">Cancelado <?php echo $num; ?></font></p>
						
					</div>
				</div>
				<div class="col-sm-3">
				<!--	
					<div class="caja-inicio">
						<p><font color="#000">Transferencias</font></p>  <?php /* DUDA sobre la base de datos, como se cuantas transferencias en total hay ¿? */ ?>
						<p><font color="#005952" size="6">?</font></p> 
						<p><font size="1">En tramite ?</font></p> <?php /* DUDA sobre la base de datos, como se cuantas transferencias en tramite hay ¿? */ ?>
						<p><font size="1">Por confirmar ?</font></p> <?php /* DUDA sobre la base de datos, como se cuantas transferencias por confirmar hay ¿? */ ?>
						<p><font size="1">Recibidas ?</font></p> <?php /* DUDA sobre la base de datos, como se cuantas transferencias recibidas hay ¿? */ ?>
						<p><font size="1">Enviadas ?</font></p> <?php /* DUDA sobre la base de datos, como se cuantas transferencias enviadas hay ¿? */ ?>
					</div>
				-->
				</div>
			</div>	
			<?php } ?><div style="height:100px;"></div>
		</div>
    </div>
<div class="footer"><b>Soporte:</b> jpgl.garrido.linares@gmail.com</div>
</div>
</body></html>
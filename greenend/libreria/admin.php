<?php

class admin extends MySQL {
public function classmsj($msj){ if(strlen($msj) == 5){ return 'alert-danger'; }else{ if(substr($msj,0,1) == '1'){ return 'alert-danger'; }else{ return 'alert-success'; } } }
public function htmlop($msj){ $arr_arch = array(1=>"usuario",2=>"pedidos",3=>"transferencia",4=>"producto",5=>"stock",6=>"importacion",7=>"grupo",8=>"config"); return $arr_arch[$msj]; }
public function htmlopb($msj){ if($msj == "ppal"){ return "Menu principal"; }else{ $arr_arch = array(1=>"Usuarios",2=>"Pedidos",3=>"Transferencias",4=>"Productos en liquidación",5=>"Green Stock",6=>"Listas de importaci&oacute;n",7=>"Grupos de compra",8=>"Configuraci&oacute;n"); return $arr_arch[$msj]; } }
public function menuop($op){ $arr_arch = array(1=>'Cabecera'); }
public function msjusu($msj){
$arr_error = array('01'=>"<strong>¡Error!</strong> Cambios no modificados",'02'=>"Error de imagen, las extensiones permitidas son: jpg, jpeg, png, gif y bmp",'03'=>"imagen demasiado ancha",'04'=>"Orden de cajas incorrecto",'05'=>'usuario y contrase&ntilde;a deben ser mayores de 4 caracteres','05'=>"Caracteres no permitidos: 0 1 2 3 4 5 6 7 8 9 a b e c d e f g h i j q l m n o p q r s t u v w x y z - _ . A B C D E F G H A I J K L M N O P Q R S T U V W X Y Z  # * en nombres de imagenes y url",'07'=>"usuario repetido",'08'=>'CAMBIOS NO MODIFICADOS Reglas de modificar administradores: <br />Campos que no pueden estar vacios: \'Usuario\', \'Nuevo password\', \'Repite password\' y \'Email\'<br />Los campos \'Nuevo password\' y \'Repite password\' deben ser identicos<br />El campo email tiene que ser un email formato xxxx@xxxx.xxx, si escribe mal estos campos no se recibiran bien los email','09'=>'Parametros mal insertados','10'=>'NO SE HA INSERTADO, El Campo t&iacute;tulo es obligatorio','11'=>'DOCUMENTO NO SUBIDO, Los tipos de documentos han de ser zip, rar o pdf','12'=>'NO INSERTADO, los campos obligatorios son titulo, texto y estilo.','13'=>'Item no insertado, revise los campos obligatorios.','14'=>'NO INSERTADO, Caracteres no permitidos','CAMBIOS NO MODIFICADOS, El campo t&iacute;tulo no puede estar vac&iacute;o.','15'=>'FORMULARIO INCORRECTO, consulte con su administrador, si este error continua y cominiquele los pasos que ha seguido, para dar con este error.','16'=>'ENLACE NO INSERTADO, los campos titulo y enlace son obligatorios.','17'=>'&Eacute;sta url no se puede eliminar','18'=>'CAMBIOS NO MODIFICADOS, EL ORDEN NO ES CORRECTO','19'=>'DOCUMENTO NO INSERTADO, los campos obligatorios son: nombre y documento','20'=>'USUARIO NO INSERTADO, Los campos Nombre y Contrase&ntilde;a deben ser mayores que 4 caracteres.','21'=>'USUARIO NO INSERTADO, ese usuario ya existe.','22'=>'ERROR. url repetida. No debe usar una url que ya existe.');  
$arr_bien = array('01'=>"<strong>¡Bien hecho!</strong> Cambios modificados",'02'=>"CAMBIOS MODIFICADOS",'03'=>'URL BORRADA','04'=>"URL NUEVA");
$arr_grave = array('91'=>"ERROR GRAVE, Contacte con el proveedore de pagina web. error 10991. ",'92'=>"ERROR GRAVE, Contacte con el proveedore de pagina web. error 10992. Pruebe restaurar valores secundarios",'93'=>"ERROR GRAVE, Posibles causas permisos de archivos, nombre repetido de cdocumento en la misma carpeta");
if(strlen($msj) == 5){ $error = $arr_grave[substr($msj,3,2)]; }else{ if(substr($msj,0,1) == '1'){ $error = $arr_error[substr($msj,2,2)]; }else{ $error = $arr_bien[substr($msj,2,2)]; } }
return $error; }

public function soloEstos($cad,$permitidos){ $e = 0; for($i=0; $i<strlen($cad); $i++){ if(strpos($permitidos, substr($cad,$i,1)) != "") $e++; }
$i--; if($e == strlen($cad)) return 1; else return 0; }	

public function menu($id){
$ret = "<ul>"; $sql = $this->consulta("select * from url_amg where seccion = '".$id."'"); 
while($reg=$this->fetch_object($sql)){ 
 $ssql = $this->consulta("select * from menu where tipo = '2' and id = '".$reg->id."'");  
 if($this->num_rows($ssql) > 0){
  $ret .= "<li>".$reg->titulo." <input type=\"checkbox\" name=\"ver".$reg->id."\" value=\"1\" checked=\"checked\" />";
  $ret .= $this->menu($reg->id);
  $ret .= "</li>";
 }else{ $ret .= "<li>".$reg->titulo." <input type=\"checkbox\" name=\"ver".$reg->id."\" value=\"1\"></li>";  }
}

$ret .= "</ul>";
return $ret; }

public function quitarprep($url){
$prep = array("-a-","-ante-","-cabe-","-con-","-contra-","-de-","-desde-","-hasta-","-para-","-por-","-junto-","-en-","-pesar-","-al-","-tras-","-sobre-","-so-","-sin-","y","que");
$url = str_replace($prep,"-",$url); return $url; }

public function cambiaralfn($dato,$punto="si"){ 
$dato = htmlentities(strtolower($dato),ENT_QUOTES,"UTF-8");
$find = array("&quot;","&amp;","&lt;","&gt;","&euro;","&nbsp;","&acute;","&AElig;","&aelig;","&brvbar;","&cedil;","&cent;","&circ;","&copy;","&curren;","&deg;","&divide;","&ETH;","&eth;","&fnof;","&frac12;","&frac14;","&frac34;","&iquest;","&laquo;","&macr;","&micro;","&middot;","&not;","&OElig;","&oelig;","&ordm;","&Oslash;","&oslash;","&para;","&plusmn;","&pound;","&raquo;","&reg;","&Scaron;","&scaron;","&sect;","&shy;","&sup1;","&sup2;","&sup3;","&szlig;","&THORN;","&thorn;","&tilde;","&times;","&uml;","&ndash;","&mdash;","&lsquo;","&rsquo;","&sbquo;","&ldquo;","&rdquo;","&bdquo;","&lsaquo;","&rsaquo;","&dagger;","&Dagger;","&permil;","&bull;","&hellip;","&Prime;","&prime;","&oline;","&frasl;","&weierp;","&image;","&real;","&trade;","&alefsym;","&larr;","&uarr;","&rarr;","&darr;","&harr;","&crarr;","&lArr;","&uArr;","&rArr;","&dArr;","&hArr;","&forall;","&part;","&exist;","&empty;","&nabla;","&isin;","&notin;","&ni;","&prod;","&sum;","&minus;","&lowast;","&radic;","&prop;","&infin;","&ang;","&and;","&or;","&cap;","&cup;","&int;","&there4;","&sim;","&cong;","&asymp;","&ne;","&equiv;","&le;","&ge;","&sub;","&sup;","&nsub;","&sube;","&supe;","&oplus;","&otimes;","&perp;","&hArr;","&sdot;","&lceil;","&rceil;","&lfloor;","&rfloor;","&lang;","&rang;","&loz;","&spades;","&clubs;","&hearts;","&diams;","&alpha;","&Beta;","&beta;","&Gamma;","&gamma;","&Delta;","&delta;","&Epsilon;","&epsilon;","&Zeta;","&zeta;","&Eta;","&eth;","&Theta;","&theta;","&thetasym;","&Iota;","&iota;","&Kappa;","&kappa;","&Lambda;","&lambda;","&Mu;","&mu;","&Nu;","&nu;","&Xi;","&xi;","&Omicron;","&omicron;","&Pi;","&pi;","&piv;","&Rho;","&rho;","&Sigma;","&sigma;","&sigmaf;","&Tau;","&tau;","&Upsilon;","&upsilon;","&upsih;","&Phi;","&phi;","&Chi;","&chi;","&Psi;","&psi;","&Omega;","&omega;","&ome","&iexcl;"); 
$dato = str_replace("&ntilde","n",$dato);
$dato = str_replace($find,"",$dato); $dato = str_replace (array("&aacute;","&agrave;","&acirc;","&atilde;","&auml;"), "a", $dato);
$dato = str_replace (array("&eacute;","&egrave;","&ecirc;","&etilde;","&euml;"), "e", $dato); $dato = str_replace (array("&iacute;","&igrave;","&icirc;","&itilde;","&iuml;"), "i", $dato);
$dato = str_replace (array("&oacute;","&ograve;","&ocirc;","&otilde;","&ouml;"), "o", $dato); $dato = str_replace (array("&uacute;","&ugrave;","&ucirc;","&utilde;","&uuml;"), "u", $dato);
$dato = str_replace (array("&Aacute;","&Agrave;","&Acirc;","&Atilde;","&Auml;"), "A", $dato); $dato = str_replace (array("&Eacute;","&Egrave;","&Ecirc;","&Etilde;","&Euml;"), "E", $dato);
$dato = str_replace (array("&Iacute;","&Igrave;","&Icirc;","&Itilde;","&Iuml;"), "I", $dato); $dato = str_replace (array("&Oacute;","&Ograve;","&Ocirc;","&Otilde;","&Ouml;"), "O", $dato); 
$dato = str_replace (array("&Uacute;","&Ugrave;","&Ucirc;","&Utilde;","&Uuml;"), "U", $dato);	
$find = array('^','´','`','¨','~','!','"','#','$','%','&','\'','(',')','*','+',',','/',':',';','=','?','@','[','\\',']','^','{','|','}','¡','¢','£','¤','¥','§','©','«','¬','®','¯','°','±','µ','¶','·','¸','»','¿','Æ','Ø','ß','æ','÷','ø'); $repl = ""; $dato = str_replace($find, $repl, $dato); $dato = str_replace(" ", "-", $dato);
if($punto == "si"){ $dato = str_replace(".", "", $dato); } return $dato; } } ?>
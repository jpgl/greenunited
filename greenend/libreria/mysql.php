<?php 
class MySQL{
  private $conexion; 
  public function MySQL(){ 
    if(!isset($this->conexion)){
      $this->conexion = mysqli_connect($GLOBALS['serverdb'],$GLOBALS['admindb'],$GLOBALS['passwddb'],$GLOBALS['tabladb']);
    }
	mysqli_query("SET NAMES 'utf8'",$this->conexion);
  }
  public function consulta($consulta){ 
    return mysqli_query($this->conexion,$consulta);
  }
  public function fetch_object($consulta){
   return mysqli_fetch_object($consulta);
  }
  public function num_rows($consulta){
   return mysqli_num_rows($consulta);
  }
  public function ultimoid(){
  	return mysqli_insert_id($this->conexion);
  }
}
?>

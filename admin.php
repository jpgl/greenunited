<?php 

/*
 *  ##1 = explicación
 *  ##2 = nombre de archivo puesto por mi
 *  ##3 = problema/s
 *  ##4 = debug
 * 
 * */


//ini_set('display_errors', 1);
error_reporting(0); 

/* ##1 - estas dos lienas de codigo sirven para ver los errore de sintaxis en php o no. el primero es para verlo, el segundo es para no verlo. es para nosotros en plan buscar bug y errores en la compilacion  */

session_start(); 

/* ##1 - esto es para que $_SESSION funcione */

//	if(!$_SESSION['userbackend'] and !$_COOKIE['userbackend']){

/* ##1 - esto es un sistema de seguridad, el cual si estan activadas de antemano no entra aqui y por tanto no ejecuta el header, que es para redirigir al login. $_session seria un login digamos 'normal' si sales y apagas el ordenador 
 * la proxima vez que entre se tendrá que logear otra vez. $_COOKIE se activaria con el tipico 'recuerdame', asi si el usuario no borra las cookies del ordenador, cada vez que entre en el administrador, no tendrá que registrarse,
 * entrara sin necesidad de logearse durante dias...
 * */

//		header("location: index.php?error=3333"); exit(); 

/* ##1 - esta linea de codigo redirige al login, ?error=3333 identifica que no esta logeado y tiene que logearse para entrar aqui. */
/* ##2 - aqui el login lo he puesto index.php  */

//	} 

// include("libreria/iniciar.php"); include('../clase/admin.php'); $ad = new admin();

/* ##1 - aqui incluyo las clases de php necesarias y las inicio */
/* ##2 - en este caso las clases estan en dos carpetas, una es libreria y otra clase , un archivo para el admin seria iniciar.php, y otro que yo suelo usar tanto en el admin como en la web que se llama admin.php */

// $idadmin = $_SESSION['userbackend'] ? $_SESSION['userbackend'] : $_COOKIE['userbackend'];
// $lvl = $ini->selectcampo($idadmin,'usuarios','lvl'); 
$lvl = 10;
/* ##1 - aqui (en la primera fila) cojo el id del usuario logeado, guardado o bien desde la cookie o bien desde la session y lo guardo en una variable, con el id (segunda linea) y una funcion que suelo usar obtengo el nivel del usuario logeado */
/* ##2 - usuarios seria el nombre de la tabla usuarios administradores, 
 * lvl seria el campo del nivel de seguridad que tiene, 
 * $ini->selectcampo es una funcion de una clase que explico en otro archivo 
 * ##4 - pongo $lvl a 10 para probar el funcionamiento de un superadministrador
 * */
 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es" xml:lang="es" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Panel administrador : GreenUnited</title>
<meta name="robots" content="noindex, nofollow"> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php /* ##1 - hasta aqui el tipico inicio de html para el panel de administración */ ?>

<link href="libreria/style.css" type="text/css" rel="stylesheet">
<?php
/* ##1 - este css por si necesitamos algo especifico de css en el administrador */
/* ##2 - archivo style.css en carpeta libreria */
?>

<?php
// if(file_exists("cabecera/".$ad->htmlop($_GET['op']).".php")){ include("cabecera/".$ad->htmlop($_GET['op']).".php"); } 

/* ##1 - esto lo que hace es que carga un archivo si existe, si necesitamos un php específico que no se carga en ninguna otra seccion del administrador, lo podemos cargar insertando un archivo php en la carpeta cabecera, 
 * el nombre del archivo seria uno que escojamos previamente desde la clase admin que veras en otro archivo mejor explicado y que seguiras entendiendo mas abajo. 
 * ##2 - carpeta cabecera, 
 * $ad->htmlop() es una funcion que dada una variable numerica, devuelve el nombre del archivo que necesitamos para cada seccion
 * $_GET['op'] cada seccion iria vinculada a un numero, ejemplo: ?op=2 seria seccion 2.
 * */
?>
	
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="libreria/bootstrap.min.css" rel="stylesheet">
<link href="libreria/metisMenu.min.css" rel="stylesheet">
<link href="libreria/timeline.css" rel="stylesheet">
<link href="libreria/startmin.css" rel="stylesheet">
<link href="libreria/morris.css" rel="stylesheet">
<link href="libreria/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<?php /* ##1 - hasta aqui los css que necesitamos para bootstrap */ ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>
<script src="libreria/bootstrap.min.js"></script>
<script src="libreria/metisMenu.min.js"></script>
<script src="libreria/startmin.js"></script>


<?php /* ##1 - hasta aqui los js que necesitamos para bootstrap */ ?>


</head>
<body>




<div id="wrapper">
<?php /* ##1 - aqui se inicia el administrador al completo  */ ?>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <?php /* ##1 - aqui se inicia la cabecera y la columna izquiera con el menu de secciones */ ?>
        
        
        <div class="navbar-header dasboar">
           <i class="fa fa-map-signs fa-fw"></i> <font class="fontb">GreenUnited</font>
        </div>
        <?php 
		 
		 /* ##1 - hasta aqui el nombre en la esquina superior izquierda. 
		  * ##3 - No encuentro el icono que se muestra en el diseño, he puesto ese, como se puede poner cualquier icono que hay en la lista_de_iconos que te mando
		  * */ ?>
        
        
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Menu movil</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        
        <?php /* ##1 - hasta aqui el sistema responsive para el menu de la columna izquierda */ ?>
        

        <ul class="nav navbar-right navbar-top-links">
        <?php /* ##1 - aqui empieza el menu superior derecho */ ?>
        
        
        
            <li class="dropdown">
            <?php /* ##1 - aqui empieza el primer icono (usuario) que dara las opciones basicas del perfil de usuario logeado. */ ?>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>  <b class="caret"></b>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="admin.php?op=12&se=2"><i class="fa fa-user fa-fw"></i> Perfil</a>
                    </li>
                    <li><a href="admin.php?op=12&se=7&id=<?php echo $idadmin; ?>"><i class="fa fa-gear fa-fw"></i> Configurar</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="libreria/unloggin.php?user=adm"><i class="fa fa-sign-out fa-fw"></i> Desconectar</a>
                    </li>
                </ul>
            </li>
            <?php /* ##2 - admin.php?op=12 iria a la seccion editar perfil &se=2 significa la sub seccion de editar perfil que seria el perfil  */ ?>
            
            
        
            <li class="dropdown messages-menu">
            <?php /* ##1 - aqui empieza el segundo icono (campana) que dara la alerta sobre eventos en la base de datos. */ ?>
               



                	<?php 
                	
                	/* ##1 - aqui empieza el sistema de alertas, se basa en la base de datos, el campo estado indica si necesita aprobar, aceptar, controlar...
					 * yo suelo poner estado = 0 cuando la alerta esta activada, estado = 1 cuando todo esta ok, estado = 2 cuando no esta ok, pero no hay alerta...
					 * el campo fecha es importante, para indicar desde cuando es el evento o la alerta.
					 *  */
                	
                	// $alertarray = array();
                	// $alert = false;
                	 
					/* ##1 - $alert tiene que ver con alerta en general, asi se activara el color rojo en la campana */
					
					// $alertliquidacion = false;
					
					/* ##1 - alerta en productos en liquidación concretamente, asi se pondrá esa parte del menu tambien en rojo. */
					
					// $alertimportacion = false;
					
					/* ##1 - */
					
					// $alerthospital = false;
					// $alertusuario = false;
					// $alertcom = false;
					// $alertesp = false;
                	// $sql = $ini->consulta("select id, fecha from opinion where estado = '0'"); if($ini->num_rows($sql) > 0){
                		
					//	 $alert = true; $alertopinion = true;
					//	 while($reg = $ini->fetch_object($sql)){
					//	 	$alertarray[] = array(0=>"Nueva opinión",1=>$reg->fecha,2=>6,3=>3,4=>$reg->id);
					//	 } 
					
					// }
					// $sql = $ini->consulta("select * from doctores where tipo = '0'"); if($ini->num_rows($sql) > 0){
						 	
					//	  $alert = true; $alertdoctor = true;
					//	  while($reg = $ini->fetch_object($sql)){
					//	  	 $alertarray[] = array(0=>"Nuevo doctor",1=>$reg->fecha,2=>7,3=>7,4=>$reg->id);
					//	  }
						 
					// }
					// $sql = $ini->consulta("select * from hospital where tipo = '0'"); if($ini->num_rows($sql) > 0){
						
					//	  $alert = true; $alerthospital = true; 
					//	  while($reg = $ini->fetch_object($sql)){
					//	  	  $alertarray[] = array(0=>"Nuevo hospital",1=>$reg->fecha,2=>8,3=>7,4=>$reg->id);
					//	  }
					// }
					// $sql = $ini->consulta("select * from usuarios_web where tipo = '0'"); if($ini->num_rows($sql) > 0){
						 	
					//	  $alert = true; $alertusuario = true;
					//	  while($reg = $ini->fetch_object($sql)){
					//	      $alertarray[] = array(0=>"Nuevo usuario",1=>$reg->fecha,2=>9,3=>7,4=>$reg->id);
					//	  }
					// }
					// $sql = $ini->consulta("select * from compania where tipo = '0'"); if($ini->num_rows($sql) > 0){
						 	
					//	  $alert = true; $alertcom = true;
					//	  while($reg = $ini->fetch_object($sql)){
					//	  	  $alertarray[] = array(0=>"Nueva compañia",1=>$reg->fecha,2=>10,3=>7,4=>$reg->id);
					//	  }
					// }
					// $sql = $ini->consulta("select * from especialidad where tipo = '0'"); if($ini->num_rows($sql) > 0){
						  	
					//	  $alert = true; $alertesp = true; 
					//	  while($reg = $ini->fetch_object($sql)){
					//	  	  $alertarray[] = array(0=>"Nueva especialidad",1=>$reg->fecha,2=>11,3=>7,4=>$reg->id);
					//	  }
					
					// }
					
					
					/* 
					 * ##1 - hasta aqui el sistema de variables alertadas, e inserccion de array de alertas
					 * ##2 - uso las clases $ini->consulta para hacer consultas sql, 
					 * uso $ini->fetch_object para hacer el bucle de los resultados del sql
					 * uso $ini->num_rows para saber el numero de resultados del sql
					 * ##3 - no estaba preparado para el numero de alertas, pero es facil, seria crear otra variable e ir incrementandola con el numero de resultado de las consultas, lo veo facil, seria una mejora
					 * 
					 * */
					
					
                	?>



         
         <?php 
         
         $alert = true; 
         /* ##4 - pongo $alert a true para probar el sistema de alerta */
         
         
         $alertarray[] = array(0=>"Alerta de ejemplo 2",1=>"2017-08-04 11:34:29",2=>7,3=>8,4=>4);
         $alertarray[] = array(0=>"Alerta de ejemplo 1",1=>"2017-05-25 12:34:29",2=>7,3=>8,4=>4);
		 
         
		 
         /* ##4 - hago un array de resultados de alertas para ver como quedaria
		  * ##1 - el array va asi, el elemento 0 es el titulo de la alerta. elemento 1 es la fecha desde que se creo la alerta. elemento 2 es la seccion a donde dirige el enlace. 
		  * elemento 3 es la subseccion del enlace, elemento 4 es el id del registro para entrar en la seccion , subseccion e id concretos.
		  * 
		  * */
         
         
         ?>
          <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded href="#">
          <i <?php if($alert){ ?>style="color:#d9534f;" <?php } ?>class="fa fa-bell fa-fw"></i> <b <?php if($alert){ ?>style="color:#d9534f;" <?php } ?>class="caret"></b> <span class="label label-danger">5</span>
          </a>
          
          <?php
          /* ## 1 - este enlace es la campanita los if($alert) son si se pone la campanita en rojo o no. el 5 dentro de span class label label-danger todavia no esta desarrollado, 
           * pero como digo es cuestion de hacer un contador e ir incrementandolo con los num_rows
		   * 
           */
          ?>
                <?php if($alert){  $fechaahora = date("Y-m-d H:i:s"); ?>
                <ul class="dropdown-menu dropdown-alerts">
                	
                <?php 
 
 				/* ## 1 - aqui empieza la lista de alertas, sacada de un mismo array que se harian de los sql de las tablas que necesitemos
				 * 
				 * 
				 * */	
 					
 				
 					
 
 						$aux = array();
						foreach($alertarray as $idid=>$valid){ $aux[$idid] = $valid[1]; }
						 array_multisort($aux, SORT_DESC, $alertarray); 
 						/* ##1 - asi es como ordeno por fecha el array que viene desordenado, ya que primero se hace de una tabla y lueo de otra... */
						
						
						foreach($alertarray as $val){
						/* ##1 - esto abre el bucle de la lista de alertas */
						
						$fecha1 = new DateTime($val[1]);
						$fecha2 = new DateTime($fechaahora);
						$fechadiff = $fecha1->diff($fecha2);
						/* ##1 - esto es para sacar cuantos asño, meses, semanas, dias, horas, minutos y segundos hace desde que se creo la alerta comparandolo con la fecha y hora actual */
						?>
                    <li>
                        <a href="admin.php?op=<?php echo $val[2]; ?>&se=<?php echo $val[3]; ?>&id=<?php echo $val[4]; ?>">
                            <div>
                                <i class="fa fa-comment fa-fw"></i> <?php echo $val[0]; ?>
                                <span class="pull-right text-muted small">Hace <?php if($fechadiff->y > 0){ echo " ".$fechadiff->y." años, "; } if($fechadiff->m > 0){ echo " ".$fechadiff->m." meses, "; } if($fechadiff->d > 0){ echo " ".$fechadiff->d." dias, "; } if($fechadiff->h > 0){ echo " ".$fechadiff->h." horas, "; } if($fechadiff->i > 0){ echo " ".$fechadiff->i." minutos, "; } if($fechadiff->s > 0){ echo " ".$fechadiff->s." segundos. "; } ?></span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <?php }  ?>

                </ul>
                <?php } ?>
            </li>
            
            
            




        </ul>
        
        
        
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="user-profile">
                        <div class="dropdown user-pro-body">
                        	
                        	<img src="imagen/yo-perfil.gif" class="img-circle">
                        	
                        	<span><font><b>Bienvenido</b> <?php // echo $ini->selectcampo($_SESSION['userbazcach'],'usuarios','usu'); ?>Usuario</font></span>
                        	
                        	<?php 
							 
							 /* 
							  * 
							  * */  ?>
                        	
                        </div>
                    </li>
                    
                    <?php if($lvl > 9 or $lvl == 4){ ?>
                    <li>
                        <a href="admin.php?op=12" class="active"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <?php } ?>
                    
                    <?php if($lvl > 8){ ?>
                    <li>
                        <a href="#"><i class="fa fa-user fa-fw"></i> Usuarios<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="admin.php?op=3&se=3"><i class="fa fa-eye fa-fw"></i> Ver Usuarios</a></li>
                            <li><a href="admin.php?op=3&se=1"><i class="fa fa-terminal fa-fw"></i> Crear Usuarios </a></li>
                            <li><a href="admin.php?op=3&se=2"><i class="fa fa-edit fa-fw"></i> Modificar usuarios </a></li>  
                            <li><a href="admin.php?op=3&se=4"><i class="fa fa-recycle fa-fw"></i> Eliminar usuarios </a></li>                          
                        </ul>
                    </li>
                    <?php } ?>
                    
                    <?php if($lvl > 8){ ?>
                    <li>
                        <a href="#"><i class="fa fa-shopping-cart fa-fw"></i> Pedidos<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="admin.php?op=1&se=1"><i class="fa fa-eye fa-fw"></i> Ver pedidos</a></li>
                            <li><a href="admin.php?op=1&se=2"><i class="fa fa-edit fa-fw"></i> Modificar pedidos </a></li>                         
                        </ul>
                    </li>
                    <?php } ?>
                    
                    <?php if($lvl > 7){ ?>
					<li>
						<a href="#"><i class="fa fa-exchange fa-fw"></i> Transferencias<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="admin.php?op=4&se=1"><i class="fa fa-terminal fa-fw"></i> Crear transferencias</a></li>
                            <li><a href="admin.php?op=4&se=2"><i class="fa fa-edit fa-fw"></i> Modificar transferencias </a></li>
                            <li><a href="admin.php?op=4&se=3"><i class="fa fa-recycle fa-fw"></i> Eliminar transferencias </a></li>
                            <li style="display:none;"><a href="admin.php?op=4&se=6">no</a></li>
                            <li style="display:none;"><a href="admin.php?op=4&se=5">no</a></li> 
						</ul>
					</li>
					<?php } ?>
					
					<?php if($lvl > 7){ ?>
                    <li>
                        <a href="#"><i class="fa fa-ticket fa-fw"></i> Productos en liquidación<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="admin.php?op=5&se=1"><i class="fa fa-eye fa-fw"></i> Ver P. liquidación</a></li>
                            <li><a href="admin.php?op=5&se=2"><i class="fa fa-download fa-fw"></i> Insertar P. liquidación </a></li> 
                            <li><a href="admin.php?op=5&se=4"><i class="fa fa-recycle fa-fw"></i> Eliminar P. liquidación </a></li>
                            <li style="display:none;"><a href="admin.php?op=5&se=3">no</a></li>                         
                        </ul>
                    </li>
                    <?php } ?>
                    
                    <?php if($lvl > 7){ ?>
                    <li>
                        <a href="#"><i class="fa fa-superscript fa-fw"></i> Green stocks<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="admin.php?op=6&se=1"><i class="fa fa-eye fa-fw"></i> Ver G. stocks</a></li>
                            <li><a href="admin.php?op=6&se=2"><i class="fa fa-terminal fa-fw"></i> Crear G. stocks </a></li>
                            <li><a<?php if($alertopinion){ ?> style="color:#FF0000;"<?php } ?> href="admin.php?op=6&se=3"><i class="fa fa-legal fa-fw"></i> Aprobar G. stocks </a></li>  
                            <li><a href="admin.php?op=6&se=4"><i class="fa fa-recycle fa-fw"></i> Eliminar G. stocks </a></li>
                            <li><a href="admin.php?op=6&se=5"><i class="fa fa-expeditedssl fa-fw"></i> Administrar G. stocks </a></li>
                            <li style="display:none;"><a href="admin.php?op=6&se=6">no</a></li>
                        </ul>
                    </li>
                    <?php } ?>
                    
                    <?php if($lvl > 7){ ?>
                    <li>
                        <a href="#"><i class="fa fa-list-alt fa-fw"></i> Listas de importación<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="admin.php?op=7&se=1"><i class="fa fa-eye fa-fw"></i> Ver L. importación</a></li>
                            <li><a href="admin.php?op=7&se=2"><i class="fa fa-terminal fa-fw"></i> Crear L. importación </a></li> 
                            <li><a href="admin.php?op=7&se=3"><i class="fa fa-edit fa-fw"></i> Modificar L. importación </a></li>  
                            <li><a href="admin.php?op=7&se=4"><i class="fa fa-recycle fa-fw"></i> Eliminar L. importación </a></li>
                            <li style="display:none;"><a href="admin.php?op=7&se=8"><i class="fa fa-expeditedssl fa-fw"></i> L. importación cancelados </a></li>
                            <li style="display:none;"><a href="admin.php?op=7&se=5">no</a></li>
                            <li style="display:none;"><a href="admin.php?op=7&se=6">no</a></li>
                        </ul>
                    </li>
                    <?php } ?>
                    
                    <?php if($lvl > 7){ ?>
                    <li>
                        <a href="#"><i class="fa fa-users fa-fw"></i> Grupos de compra<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="admin.php?op=8&se=1"><i class="fa fa-eye fa-fw"></i> Ver G. compra</a></li>
                            <li><a href="admin.php?op=8&se=2"><i class="fa fa-terminal fa-fw"></i> Crear G. compra </a></li>
                            <li><a<?php if($alerthospital){ ?> style="color:#FF0000;"<?php } ?> href="admin.php?op=8&se=7"><i class="fa fa-legal fa-fw"></i> Administrar G. compra </a></li> 
                            <li><a href="admin.php?op=8&se=3"><i class="fa fa-edit fa-fw"></i> Modificar G. compra </a></li>  
                            <li><a href="admin.php?op=8&se=4"><i class="fa fa-recycle fa-fw"></i> Eliminar G. compra </a></li>
                            <li style="display:none;"><a href="admin.php?op=8&se=8"><i class="fa fa-expeditedssl fa-fw"></i> G. compra cancelados </a></li>
                            <li style="display:none;"><a href="admin.php?op=8&se=5">no</a></li>
                            <li style="display:none;"><a href="admin.php?op=8&se=6">no</a></li>
                        </ul>
                    </li>
                    <?php } ?>
                    
                    <?php if($lvl > 7){ ?>
                    <li>
                        <a href="#"><i class="fa fa-gear fa-fw"></i> Configuración<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="admin.php?op=9&se=1"><i class="fa fa-eye fa-fw"></i> Ver visitas</a></li>
                            <li><a href="admin.php?op=9&se=2"><i class="fa fa-terminal fa-fw"></i> Códigos de seguimiento </a></li>
                        </ul>
                    </li>
                    <?php } ?>
                    

         
                    
                </ul>
            </div>
        </div>
    </nav>
    
    
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php if($_GET['op'] != ""){
                    	//  echo $ad->htmlopb($_GET['op']);
                    	 }else{
                    	 	// echo $ad->htmlopb("ppal"); 
                    	 	 } ?></h1>
                </div>
            </div>
			<?php if($_GET['msj'] != ""){ ?>
			<div class="alert <?php // echo $ad->classmsj($_GET['msj']); ?> alert-dismissable">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php // echo $ad->msjusu($_GET['msj']); ?>
			</div>
			<?php }else{ ?>
			<div style="height:100px;"></div>
			<?php } ?>
			<?php // if(file_exists("html/".$ad->htmlop($_GET['op']).".php")){
				  // include("html/".$ad->htmlop($_GET['op']).".php");
			      // }else{ ?>
			
			
			<div class="form-group row">
				<div class="col-sm-3">
					<div class="caja-inicio">
						<p><font color="#000">Usuarios</font></p>
						<p><font color="#005952" size="6">123</font></p>
						<p><font size="1">España 123</font></p>
						<p><font size="1">Chile 123</font></p>
						<p><font size="1">País 123</font></p>
						<p><font size="1">País 123</font></p>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="caja-inicio">
						<p><font color="#000">Stocks</font></p>
						<p><font color="#005952" size="6">123</font></p>
						<p><font size="1">P. Liquidación 123</font></p>
						<p><font size="1">Green stocks 123</font></p>
						<p><font size="1">Listas importación 123</font></p>
						<p><font size="1">Grupos de compra 123</font></p>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="caja-inicio">
						<p><font color="#000">Pedidos</font></p>
						<p><font color="#005952" size="6">123</font></p>
						<p><font size="1">En tramite 123</font></p>
						<p><font size="1">Pendiente pago 123</font></p>
						<p><font size="1">Por calcular 123</font></p>
						<p><font size="1">En seguimiento 123</font></p>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="caja-inicio">
						<p><font color="#000">Transferencias</font></p>
						<p><font color="#005952" size="6">123</font></p>
						<p><font size="1">En tramite 123</font></p>
						<p><font size="1">Por confirmar 123</font></p>
						<p><font size="1">Recibidas 123</font></p>
						<p><font size="1">Enviadas 123</font></p>
					</div>
				</div>
			</div>
			
			<?php // } ?><div style="height:100px;"></div>
		</div>
    </div>
    
<div class="footer"> <b>Soporte:</b> jpgl.garrido.linares@gmail.com <?php /* aqui suelo poner la url de nuestra empresa */ ?></div>


</div>



</body></html>





